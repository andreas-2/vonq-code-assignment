<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:template match="/">
        <h1>Task b: Using an XSL transformation, extract and output the list of the job titles.</h1>
        <table>
            <tbody>
                <xsl:for-each select="jobs/job">
                    <xsl:sort select="responsibleUser"/>
                    <tr>
                        <td>
                            <xsl:value-of select="name"/>
                        </td>
                        <td>
                            <xsl:value-of select="responsibleUser"/>
                        </td>
                    </tr>
                </xsl:for-each>
            </tbody>
        </table>
    </xsl:template>
</xsl:stylesheet>
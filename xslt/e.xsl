<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:template match="/">
        <response>
            <xsl:for-each select="jobs/job">
                <xsl:sort select="responsibleUser"/>
                <name>
                    <xsl:value-of select="name"/>
                </name>
                <responsible>
                    <xsl:value-of select="concat(responsibleUser, ' ', responsibleEmail)"/>
                </responsible>
            </xsl:for-each>
        </response>
    </xsl:template>
</xsl:stylesheet>
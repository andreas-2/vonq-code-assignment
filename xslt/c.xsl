<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:template match="/">
        <h1>Task c: Using an XSL transformation, filter the result to only match job titles for jobs with occupationType Freelancer.</h1>
        <table>
            <tbody>
                <xsl:for-each select="jobs/job">
                    <xsl:if test="occupationType='Freelancer'">
                        <tr>
                            <td><xsl:value-of select="name"/></td>
                        </tr>
                    </xsl:if>
                </xsl:for-each>
            </tbody>
        </table>
    </xsl:template>
</xsl:stylesheet>
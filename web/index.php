<?php

use Symfony\Component\HttpFoundation\Response;

require_once __DIR__ . "/../vendor/autoload.php";

$app = new Silex\Application();

$xml = new DOMDocument();
$xml->load("https://preview05.kandidatenportal.eu/Syndication/Xml?referrer=Betsy&culture=de");

$app->get("/b", function () use ($xml) {
    $xsl = new DOMDocument();
    $xsl->load(__DIR__ . "/../xslt/b.xsl");

    $processor = new XSLTProcessor();
    $processor->importStylesheet($xsl);

    return $processor->transformToXml($xml);
});

$app->get("/c", function () use ($xml) {
    $xsl = new DOMDocument();
    $xsl->load(__DIR__ . "/../xslt/c.xsl");

    $processor = new XSLTProcessor();
    $processor->importStylesheet($xsl);

    return $processor->transformToXml($xml);
});

$app->get("/d", function () use ($xml) {
    $xsl = new DOMDocument();
    $xsl->load(__DIR__ . "/../xslt/d.xsl");

    $processor = new XSLTProcessor();
    $processor->importStylesheet($xsl);

    return $processor->transformToXml($xml);
});

$app->get("/e", function () use ($xml) {
    $xsl = new DOMDocument();
    $xsl->load(__DIR__ . "/../xslt/e.xsl");

    $processor = new XSLTProcessor();
    $processor->importStylesheet($xsl);

    return Response::create(
        $processor->transformToXml($xml),
        200,
        [
            "Content-Type" => "xml"
        ]
    );
});

$app->run();

return $app;
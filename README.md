# Install dependencies
Run `composer install`

# Run unit tests
Run `phpunit`

# Run the app
You can run the app using PHP's built-in web server: `php -S 127.0.0.1:8080 -t web`. To view task b, you can then open `127.0.0.1:8080/b` in your browser.

If you use your own web server, this is how you can test the tasks:

* http://<server-address>/<directory>/web/index.php/b
* http://<server-address>/<directory>/web/index.php/c
* http://<server-address>/<directory>/web/index.php/d
* http://<server-address>/<directory>/web/index.php/e

Thank you for reviewing my submission. Have a great day!
<?php

use Silex\WebTestCase;

final class XslTest extends WebTestCase
{
    public function createApplication()
    {
        $app = require __DIR__ . "/../web/index.php";
        $app["debug"] = true;
        unset($app["exception_handler"]);
        return $app;
    }

    public function testTaskB()
    {
        $client = $this->createClient();
        $crawler = $client->request("GET", "/b");

        $this->assertCount(9, $crawler->filter("tr"));
    }

    public function testTaskC()
    {
        $client = $this->createClient();
        $crawler = $client->request("GET", "/c");

        $this->assertCount(2, $crawler->filter("tr"));
    }

    public function testTaskD()
    {
        $client = $this->createClient();
        $crawler = $client->request("GET", "/d");

        $this->assertEquals("Testjob-CV", $crawler->filter("tr > td")->first()->text());
    }
}